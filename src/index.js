import Vue from 'vue';
import App from './vues/App.vue';
import twemoji from 'twemoji';

new Vue(App).$mount('#bucksplayer');
twemoji.parse(document.body);
